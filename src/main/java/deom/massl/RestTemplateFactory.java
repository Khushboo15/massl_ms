package deom.massl;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * This class allows you to simply build a RestTemplate instance that can be used to invoke other MS with MASSL
 *
 * The way to use it would be to create an @Configuration bean that will create a bean by
 * calling one of the methods in this factory.
 *
 * @author d874905
 */

@Service
public class RestTemplateFactory {
	
	@Value("${http.client.ssl.key-store}")
	private Resource keyStore;

	@Value("${http.client.ssl.key-store-password}")
	private String keyStorePassword;
	
	@Value("${http.client.ssl.trust-store}")
	private Resource trustStore;

	@Value("${http.client.ssl.trust-store-password}")
	private String trustStorePassword;
	
    private static final Logger LOG = LoggerFactory.getLogger(RestTemplateFactory.class);
    
    public RestTemplate buildSslRestTemplate() {
    	LOG.info("Building RestTemplate with MASSL");
        RestTemplate restTemplate = new RestTemplate();
        try
        {	     
             SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                new SSLContextBuilder()
     	        .loadKeyMaterial(
    	            keyStore.getFile(),    	           
    	            keyStorePassword.toCharArray(),keyStorePassword.toCharArray() )
    	        .loadTrustMaterial(
    	            trustStore.getFile(),
    	            trustStorePassword.toCharArray(),
      	            new TrustSelfSignedStrategy())
    	        .build());
             HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
             restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
             LOG.info("SSL Context built successfully. ");
        }
        catch(Exception e)
        {
        	LOG.error("Error while building SSL Context",e);
        }

        restTemplate.setErrorHandler(new MicroserviceResponseErrorHandler());
        
        LOG.info("Builded RestTemplate with MASSL");
        
        return restTemplate;
    }
}
