package deom.massl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import demo.util.GetOAuthToken;

@SpringBootApplication
public class MasslApplication {
	
	@Autowired
	RestTemplateFactory restTemplateFactory;
	
	@Bean
	public RestTemplate restTemplateWithSsl() {
		return restTemplateFactory.buildSslRestTemplate();
	}
	
	@Bean
	public GetOAuthToken getOAuthToken() {
		return new GetOAuthToken();
	}

	public static void main(String[] args) {
		SpringApplication.run(MasslApplication.class, args);
	}
}
