package deom.massl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import demo.util.GetOAuthToken;


@RestController
public class Controller {
	
	@Autowired
	RestTemplate restTemplateWithSsl;
	
	@Autowired
	GetOAuthToken getOAuthToken;

	@GetMapping("/test")
	public String getResp() {
		
		String token =getOAuthToken.getAuthToken().getAccessToken();
		HttpHeaders header = new HttpHeaders();
		header.set("Authorization", "Bearer "+token);
		HttpEntity<String> request = new HttpEntity<>(
				null, header);
		header.set("Content-Type", "application/json");
		ResponseEntity<String> exchange = restTemplateWithSsl.exchange("https://massl.org006.t-dev.corp.telstra.com/common", HttpMethod.GET, request, String.class);
		System.out.println(exchange);
		return exchange.getBody();
	}
	
}
