package demo.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class GetOAuthToken {
	
	@Value("${oauth.URL}")
	private String oAuthURL;
	
	@Value("${oauth.clientId}")
	private String oAuthClientId;
	
	@Value("${oauth.client.secret}")
	private String oAuthClientSecret;
	
	@Value("${oauth.grant_type}")
	private String oAuthGrantType;
	
	@Value("${oauth.scope}")
	private String oAuthScope;
	
	@Autowired
	RestTemplate restTemplate;
	
	public OAuthResponse getAuthToken(){
		ResponseEntity<OAuthResponse> response = restTemplate.exchange(oAuthURL, HttpMethod.POST, createRequestObject(), OAuthResponse.class);
		return response.getBody();
	}
	
	private HttpEntity<MultiValueMap<String, String>> createRequestObject(){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> requestPayload= new LinkedMultiValueMap<>();
		requestPayload.add("client_id", oAuthClientId);
		requestPayload.add("client_secret", oAuthClientSecret);
		requestPayload.add("grant_type", oAuthGrantType);
		requestPayload.add("scope", oAuthScope);
		return new HttpEntity<>(requestPayload,headers);
	}

}